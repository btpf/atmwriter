
exports.generateSheet = function(results){

    var ws_data = [
        [ ],
        ["DATE"],
        ["", "Location 1", "Location 1 Fee"],
        [""],
      ];
      for(let i = 0; i < 31; i++){
        let date = "";
        if(i <= lastMonthDays.length) date = lastMonthDays[i]
        ws_data.push([{ t:'d', v: date, z:"MMM DD"}])
      }
      
    results.map((item)=>{
        item.fee = parseFloat(item.fee.replace(/[$,]+/g,""))
        item.amount = parseFloat(item.amount.replace(/[$,]+/g,""))
      })
  
  
  
  
      let locationList = []
      results.forEach((item)=>{
        if(!locationList.includes(item.location)){
          locationList.push(item.location)
        }
      })
  
      console.log(locationList);
  
  
       let cleanResults = []
  
       let endOfArray = results.length;
       let workingPosition = 0; //Clean results is the first
  
  
       cleanResults.push({...results[0]}) // Base case
       for(let i = 1; i < endOfArray; i++){
  
        // If location or date are different, add the next item from results, and switch that to the working index.
         if(
           cleanResults[workingPosition].location != results[i].location ||
           cleanResults[workingPosition].date != results[i].date){
  
           cleanResults.push({...results[i]});
           workingPosition += 1;
         }else if(cleanResults[workingPosition].location == results[i].location && cleanResults[workingPosition].date == results[i].date){
           cleanResults[workingPosition].fee += results[i].fee
           cleanResults[workingPosition].amount += results[i].amount
         }
       }
       console.log(cleanResults)
  
  
       XLSX.SSF.load('#,$ ##0.00;$ [Red](#,##0.00)', 164);
       for(let i = 0; i < 31; i++){
         for(let j = 0; j < locationList.length*2;j++){
            ws_data[4+i][j + 1] = { t:'n', v: 0.00, z: XLSX.SSF.get_table()[164]}
         }
       }
       //Add names to sheet
       for(let i = 0; i < locationList.length;i++){
        ws_data[2][(i*2) + 1] = locationList[i].toUpperCase();
        ws_data[2][(i*2) + 2] = locationList[i].toUpperCase() + " FEE";
       }
      // Append to sheet
      // console.log(cleanResults[0].date.split("/")[1])
       for(let i = 0; i < cleanResults.length; i++){
         let col = 1 + locationList.indexOf(cleanResults[i].location) * 2;
         let row = 3 + parseInt(cleanResults[i].date.split("/")[1])
         console.log("Row: " + row + " Col: " + col)
         ws_data[row][col].v = cleanResults[i].amount;
         ws_data[row][col + 1].v = cleanResults[i].fee;
       }
  
  
        // console.log(cleanResults)
  
  
    var ws = XLSX.utils.aoa_to_sheet(ws_data);
      // console.log(XLSX.SSF.get_table())
     /* Add the worksheet to the workbook */
     XLSX.utils.book_append_sheet(wb, ws, ws_name);
     XLSX.writeFile(wb, 'out.xlsx');
}