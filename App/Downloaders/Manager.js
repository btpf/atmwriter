let manager = require("../Local/Manager");
let colombus = require("./Colombus");
let pairports = require("./Paireports");
const puppeteer = require('puppeteer');
let lastMonth = require("../Local/Date").lastMonth
let credentials = require('../../config.json').accounts
let headless = require('../../config.json').headless

exports.start = async (tempdownloads) => {
  console.log("Launching Browser")
  // 4/3/22 - Arch based linux distros seems to need --no-sandbox now for some reason
  // This is likely because of linux because this is the month it broke on two seperate computers
  // The --no-sandbox seemed to also fix the issue
    const browser = await puppeteer.launch({ headless: headless, args: [
      '--no-sandbox']
});
    const page = await browser.newPage();
      // https://github.com/puppeteer/puppeteer/issues/299#issuecomment-537136417
    await page._client.send('Page.setDownloadBehavior', { behavior: 'allow', downloadPath: tempdownloads }) //This is likely to break in the future
    console.log("Fake Web Browser Launched")
    console.log("---")
    await colombus.download(page, credentials.columbus1.username,credentials.columbus1.password,tempdownloads, "columbus1")
    // await colombus.download(page, credentials.columbus2.username,credentials.columbus2.password,tempdownloads,"columbus2")
    // await pairports.download(page, credentials.pair.username,credentials.pair.password,tempdownloads,"pair", lastMonth)
   
    await browser.close();
  
    //cleanup code
    //fs.rmdirSync(tempdownloads, { recursive: true }); 
  };
