let manager = require("../Local/Manager");
const path = require('path');
const fs = require('fs');

exports.download = async (page, username, password, location, sheetName) =>{

  console.log("Visiting colombus with following credentials")
  console.log("username: " + username)
  console.log("password: " + password)
  //console.log("Note: If these credentials are wrong, the program will fail.")
  //console.log("You may update them in config.json")
  console.log("Logging in...")
  await page.goto('https://www.columbusdata.net/cdswebtool/login/login.aspx');
  await page.type("#UsernameTextbox", username);
  await page.type("#PasswordTextbox", password);
  await page.click("#LoginButton", {button:"left"})
  console.log("Requesting report")
  await page.goto("https://www.columbusdata.net/cdswebtool/includes/report.aspx?rptname=MS")
  await page.click("#ctl01_chkAllTerminals", {button:"left"})
  await page.click("#ddlReportFormat_Arrow", {button:"left"})
  const list = await page.$$('.rcbItem ');
  // wait for animation to play.
  await page.waitFor(500);
  list[1].click();
  await page.waitFor(250);
  //await page.screenshot({path: 'example.png'});

  await page.click("#btnView", {button:"left"})
  let filename =await manager.waitForFileToDownload(location)
  console.log("Report downloaded")
  console.log(path.resolve(location, filename))
  fs.renameSync(path.resolve(location, filename), path.resolve(location, sheetName + ".xls"))

}
