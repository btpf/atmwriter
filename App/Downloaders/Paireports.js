let manager = require("../Local/Manager");
const path = require('path');
const fs = require('fs');

exports.download = async (page, username, password, location, sheetName, dateRange) =>{

  console.log("Visiting PairePorts with following credentials")
  console.log("username: " + username)
  console.log("password: " + password)
  //console.log("Note: If these credentials are wrong, the program will fail.")
  //console.log("You may update them in config.json")
  console.log("Logging in...")
  await page.goto('https://www.paireports.com/myreports/GetLogin.event');
  await page.keyboard.type(username);
  await page.keyboard.press('Tab');
  await page.keyboard.type(password);
  await page.keyboard.press('Tab');
  await page.keyboard.press('Enter');
  await page.waitForNavigation({ waitUntil: 'networkidle2' })

  console.log("Requesting report")
  await page.goto('https://www.paireports.com/myreports/GetSimpleSummaryReport.event?ReportID=2');

  const reportLevel = await page.$('[name="F_ReportLevelID"]');
  reportLevel.click();
  await page.waitFor(500);
  await page.keyboard.press('ArrowDown');
  await page.keyboard.press('ArrowDown');

  const datebox = await page.$('[name="F_SettlementDate"]');
  datebox.click()
  await page.waitFor(250);
  await page.keyboard.down('ControlLeft');
  await page.keyboard.down('A');
  await page.waitFor(250);
  await page.keyboard.up('A');
  await page.keyboard.up('ControlLeft');
  await page.waitFor(250);
  await page.keyboard.press('Backspace');
  await page.waitFor(250);
  await page.keyboard.type(dateRange);
  await page.waitFor(250);
  const FilterButton = await page.$('[name="FilterButton"]');
  FilterButton.click();
  await page.waitForNavigation({ waitUntil: 'networkidle2' })
  try {
    await page.goto('https://www.paireports.com/myreports/GetSimpleSummaryReport.event?ReportCmd=CustomCommand&CustomCmdList=OpenCSVExcel');
    await page.waitForNavigation({ waitUntil: 'networkidle2' })
  } catch (error) {
    //console.log(error)
  }

  let filename =await manager.waitForFileToDownload(location)
  console.log("Report downloaded")
  console.log(path.resolve(location, filename))
  fs.renameSync(path.resolve(location, filename), path.resolve(location, sheetName + ".csv"))
  console.log("---")

  
}
