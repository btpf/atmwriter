
const fs = require('fs')


exports.waitForFileToDownload = async (downloadPath) =>{
    console.log('Waiting to download file...')
    let filename
    while (!filename || filename.endsWith('.crdownload') || filename.startsWith('columbus')) {
        filename = fs.readdirSync(downloadPath)[0]
        await (async () => {
            var start = new Date().getTime();
            while (new Date().getTime() < start + 500);
        })
    }
    return filename
}
