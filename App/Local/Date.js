let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);

// current month
let month = ("0" + (date_ob.getMonth() + 0)).slice(-2);

// current year
let year = date_ob.getFullYear();

var firstDay = new Date(year, date_ob.getMonth() - 1, 1);
var lastDay = new Date(year, date_ob.getMonth() , 0);

let days = [];
let today = new Date(firstDay);
days.push(today.toISOString())
while(today < lastDay){
    today.setDate(today.getDate() + 1);
    days.push(today.toISOString())
}

exports.today = year + "-" + month;
exports.lastMonth = firstDay.toLocaleDateString() + "-" + lastDay.toLocaleDateString()
exports.lastMonthDays = days