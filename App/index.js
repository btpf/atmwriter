let config = require("../config")
let today = require("./Local/Date").today
let lastMonth = require("./Local/Date").lastMonth
let lastMonthDays = require("./Local/Date").lastMonthDays
let manager = require("./Local/Manager")
let downloader = require("./Downloaders/Manager");
const path = require('path');
let tempdownloads = "./.tempdownloads";
const fs = require('fs')
const util = require('util');
let XLSX = require('xlsx');
const csv = require('csv-parser')

let tasksComplete = 0;
let savePath = config.location + "/" + today
//TODO. WHEN CODE BREAKS DUE TO 10+ ATM MACHINES
// let total = "SUM("
// Update this code to use the generateRowFormulas function

//Delete code should be here
// console.log(today)
console.log("Saving To: ", savePath)
if (!fs.existsSync(tempdownloads)) {
  fs.mkdirSync(tempdownloads);
}

if (!fs.existsSync(config.location)) {
  fs.mkdirSync(config.location);
}

if (!fs.existsSync(savePath)) {
  fs.mkdirSync(savePath);
}






(async () => {
  // return
  await downloader.start(tempdownloads);
  console.log("All reports downloaded")
  console.log("Parsing Files")

  const results = [];



  var wb = XLSX.utils.book_new();
  var ws_name = "Summary";

  // fs.createReadStream('./.tempdownloads/pair.csv')
  //   .pipe(csv())
  //   .on('data', (data) => results.push({ location: data.Location, date: data["Settlement Date"], fee: data.Surch, amount: data.Settlement }))
  //   .on('end', () => {
  //     //  return
  //     var wb = XLSX.utils.book_new();
  //     var ws_name = "Summary";

  //     let ws_data = GenerateCleanSheet();

  //     // Removes $ and , to convert into float
  //     cleanArray(results)

  //     let cleanResults = []

  //     let endOfArray = results.length;
  //     let workingPosition = 0; //Clean results is the first


  //     cleanResults.push({ ...results[0] }) // Base case
  //     for (let i = 1; i < endOfArray; i++) {

  //       // If location or date are different, add the next item from results, and switch that to the working index.
  //       if (
  //         cleanResults[workingPosition].location != results[i].location ||
  //         cleanResults[workingPosition].date != results[i].date) {

  //         cleanResults.push({ ...results[i] });
  //         workingPosition += 1;
  //       } else if (cleanResults[workingPosition].location == results[i].location && cleanResults[workingPosition].date == results[i].date) {
  //         cleanResults[workingPosition].fee += results[i].fee
  //         cleanResults[workingPosition].amount += results[i].amount
  //       }
  //     }

  //     populateSheet(ws_data, cleanResults)


  //     var ws = XLSX.utils.aoa_to_sheet(ws_data);
  //      ws['!cols'] = fitToColumn(ws_data);
  //     /* Add the worksheet to the workbook */
  //     XLSX.utils.book_append_sheet(wb, ws, ws_name);
  //     XLSX.writeFile(wb, savePath + '/PairePorts ' + today + '.xlsx');
  //     checkStatus()
  //   });

  let workbook = XLSX.readFile('./.tempdownloads/columbus1.xls');
  let completeArray = [];
  parseColumbus(workbook, completeArray);

  cleanArray(completeArray)

  // console.log(completeArray);

  completeArray = completeArray.map((item) => {
    if(item.location == "FOREVER YOUNGS FRUIT & VEGETABLES"){
      return { ...item, fee: item.fee * 0.44 }
    }
    return item;
  })

  // Removed MANANA GROCERY fees from sheet as this data is uneeded.
  // completeArray = completeArray.filter(item => item.location != "MANANA GROCERY")
  completeArray = completeArray.map((item) => {
    if(item.location == "MANANA GROCERY"){
      return { ...item, fee: item.transactions * 0.65 }
    }
    if(item.location == "GARDEN INN & SUITES JFK"){
      return { ...item, fee: item.transactions * 0.85 }
    }
    if(item.location == "GETTY BELTWAY"){
      return { ...item, fee: item.transactions * 0.65 }
    }
    return item;
  })
  // workbook = XLSX.readFile('./.tempdownloads/columbus2.xls');
  // cleanArray(completeArray)
  // parseColumbus(workbook, completeArray);


  var wb = XLSX.utils.book_new();
  var ws_name = "Summary";

  let ws_data = GenerateCleanSheet();
  populateSheet(ws_data, completeArray)
  var ws = XLSX.utils.aoa_to_sheet(ws_data);
  ws['!cols'] = fitToColumn(ws_data);
  /* Add the worksheet to the workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);
  XLSX.writeFile(wb, savePath + '/Columbus ' + today + '.xlsx');
  checkStatus()
})()



function fitToColumn(arrayOfArray) {
  // get maximum character of each column
  let numberOfColumns = arrayOfArray[2].length

  let widthArray = []
  widthArray.push({ wch:  6})
  //Gets the most amount of characters for each column
  //numberOfColumns
  for(let i = 1; i < numberOfColumns - 4; i++){
          maxCharLength = arrayOfArray[2][i].toString().length
          widthArray.push({ wch:   arrayOfArray[2][i].toString().length})
        }
        widthArray.push({ wch:  9})
        widthArray.push({ wch:  10})
        widthArray.push({ wch:  10})
        widthArray.push({ wch:  10})

return widthArray
}

// function fitToColumn(arrayOfArray) {
//   // get maximum character of each column
//   let numberOfColumns = arrayOfArray[2].length
//   let maxNum = Array(numberOfColumns)

//   let widthArray = []

//   //Gets the most amount of characters for each column
//   //numberOfColumns
//   for(let i = 0; i < numberOfColumns; i++){
//     let maxCharLength = 0;
//     for(let j = 0; j < 37; j++){
//       try {
//         if(arrayOfArray[j][i].toString().length > maxCharLength){
//           maxCharLength = arrayOfArray[j][i].toString().length
//         }
//       } catch (error) {
        
//       }
//     }
//     maxNum[i] = maxCharLength
//   }
//   console.log(maxNum)
//   for(let i = 0; i < numberOfColumns; i++){
//     //Seperator sum column Exception
//     if(i == numberOfColumns - 4){
//       widthArray.push({ wch:  10})
//     }else{
//       widthArray.push({ wch:   maxNum[i]})
//       // widthArray.push({ wch:  maxNum[i]})
//     }
//   }
// return widthArray
// }


function checkStatus() {
  tasksComplete++;
  console.log(tasksComplete + " Tasks Completed")
  if (tasksComplete == 2) {
    console.log("All Tasks Complete for: " + today)
    console.log("Deleting Temp Folders")
    // fs.rmdirSync(tempdownloads, { recursive: true });
  }
}
function cleanArray(completeArray) {
  completeArray.map((item) => {
    try {
      item.fee = parseFloat(item.fee.replace(/[$,]+/g, ""))
    } catch (error) {
    }
    try {
      item.amount = parseFloat(item.amount.replace(/[$,]+/g, ""))
    } catch (error) {
    }
  })
}
function parseColumbus(workbook, completeArray) {
  workbook.SheetNames.forEach(element => {
    try {
      for (let j = 0; workbook.Sheets[element]["B" + (13 + j)].v.includes("/"); j++) {
        completeArray.push({
          amount: workbook.Sheets[element]["H" + (13 + j)].v,
          date: workbook.Sheets[element]["B" + (13 + j)].v,
          fee: workbook.Sheets[element]["I" + (13 + j)].v,
          transactions: workbook.Sheets[element]["D" + (13 + j)].v,
          location: workbook.Sheets[element]["H6"].v,
        })
      }
    } catch (error) {

    }

  });
}

// {location, date, amount, fee}
function populateSheet(ws_data, data) {
  // XLSX.SSF.load('#,##0.00 $;[Red](#,##0.00) $', 164);
  XLSX.SSF.load('$ #,##0.00;$ [Red](#,##0.00)', 164);
  let locationList = []
  data.forEach((item) => {
    if (!locationList.includes(item.location)) {
      locationList.push(item.location)
    }
  })
  for (let i = 0; i < 31; i++) {
    for (let j = 0; j < locationList.length * 2; j++) {
      ws_data[4 + i][j + 1] = { t: 'n', v: 0.00, z: XLSX.SSF.get_table()[164] }
    }
  }
  //Add names to sheet
  for (let i = 0; i < locationList.length; i++) {
    ws_data[2][(i * 2) + 1] = locationList[i].toUpperCase();
    ws_data[2][(i * 2) + 2] = locationList[i].toUpperCase() + " FEE";
  }
  //Populate sheet 
  // Append numbers to sheet
  for (let i = 0; i < data.length; i++) {
    let col = 1 + locationList.indexOf(data[i].location) * 2;
    let row = 3 + parseInt(data[i].date.split("/")[1])

    ws_data[row][col].v = data[i].amount;
    ws_data[row][col + 1].v = data[i].fee;
  }

  // Sum Numbers (Right side)
  ws_data[2][locationList.length * 2 + 4] = { t: "w", v: "Gross" }
  ws_data[2][locationList.length * 2 + 3] = { t: "w", v: "Total" }
  ws_data[2][locationList.length * 2 + 2] = { t: "w", v: "Fee" }

  // Since im lazy to rewrite the loops and we are looping through every single cell anyway, lets collect the numbers we need for the bottom sums in this array

  let bottomSumCollector = Array((locationList.length * 2) + 3).fill("SUM(", 0, (locationList.length * 2) + 3)
  for (let i = 0; i < 31; i++) {
    let total = "SUM("
    let gross = "SUM("
    let fee = "SUM("
    for (let j = 0; j < locationList.length * 2; j++) {
      bottomSumCollector[j] += String.fromCharCode(65 + j + 1) + (i + 5) + " +"
      total += String.fromCharCode(65 + j + 1) + (i + 5) + " +"
      if (j % 2 == 0) gross += String.fromCharCode(65 + j + 1) + (i + 5) + " +"
      if (j % 2 == 1) fee += String.fromCharCode(65 + j + 1) + (i + 5) + " +"
    }
    ws_data[4 + i][locationList.length * 2 + 4] = { t: 'n', f: total.substring(0, total.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
    bottomSumCollector[(locationList.length * 2)] += String.fromCharCode(65 + locationList.length * 2 + 2) + (4 + i + 1) + " +"
    ws_data[4 + i][locationList.length * 2 + 3] = { t: 'n', f: gross.substring(0, gross.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
    bottomSumCollector[(locationList.length * 2) + 1] += String.fromCharCode(65 + locationList.length * 2 + 3) + (4 + i + 1) + " +"
    ws_data[4 + i][locationList.length * 2 + 2] = { t: 'n', f: fee.substring(0, fee.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
    bottomSumCollector[(locationList.length * 2) + 2] += String.fromCharCode(65 + locationList.length * 2 + 4) + (4 + i + 1) + " +"
    //Sum Numbers (Bottom Side)
  }

  let rightTotal = bottomSumCollector[(locationList.length * 2)];
  let rightGross = bottomSumCollector[(locationList.length * 2) + 1]
  let rightFee = bottomSumCollector[(locationList.length * 2) + 2]
  ws_data[4 + 31][locationList.length * 2 + 2] = { t: 'n', f: rightTotal.substring(0, rightTotal.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
  ws_data[4 + 31][locationList.length * 2 + 3] = { t: 'n', f: rightGross.substring(0, rightGross.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
  ws_data[4 + 31][locationList.length * 2 + 4] = { t: 'n', f: rightFee.substring(0, rightFee.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
  //Bottom Sums
  for (let i = 0; i < locationList.length * 2; i++) {
    let total = bottomSumCollector[i];
    ws_data[4 + 32][1 + i] = { t: 'n', f: total.substring(0, total.length - 1) + ")", z: XLSX.SSF.get_table()[164] }
  }
  // Bottom Right Sums
  let formulas = generateRowFormulas(locationList.length, 37, 1)
  ws_data[4 + 32][locationList.length * 2 + 2] = { t: 'n', f: formulas[0], z: XLSX.SSF.get_table()[164] }
  ws_data[4 + 32][locationList.length * 2 + 3] = { t: 'n', f: formulas[1], z: XLSX.SSF.get_table()[164] }
  ws_data[4 + 32][locationList.length * 2 + 4] = { t: 'n', f: formulas[2], z: XLSX.SSF.get_table()[164] }
  return ws_data
}



//Utility Functions


function GenerateCleanSheet() {
  let ws_data = [
    [],
    ["DATE"],
    ["", "Location 1", "Location 1 Fee"],
    [""],
  ];

  for (let i = 0; i < 31; i++) {
    let date = "";
    if (i <= lastMonthDays.length) date = lastMonthDays[i]
    ws_data.push([{ t: 'd', v: date, z: "MMM DD" }])
  }
  ws_data.push([])
  ws_data.push([])
  return ws_data
}


function generateRowFormulas(numberOfMachines, offsetR, offsetC) {
  //Offset R and C, then NumberofMachines is the range for our formulas
  //1 = all
  //2 = sum
  //3 = fees
  let formulas = Array(3).fill("SUM(", 0, 3);
  for (let i = 0; i < numberOfMachines * 2; i++) {
    formulas[0] += letterify(i + offsetC) + offsetR + " +"
    if (i % 2 == 0) formulas[1] += letterify(i + offsetC) + offsetR + " +"
    if (i % 2 == 1) formulas[2] += letterify(i + offsetC) + offsetR + " +"
  }
  formulas[0] = formulas[0].substring(0, formulas[0].length - 1) + ")"
  formulas[1] = formulas[1].substring(0, formulas[1].length - 1) + ")"
  formulas[2] = formulas[2].substring(0, formulas[2].length - 1) + ")"
  return formulas
}



function letterify(num) {
  let numberOfLetters = parseInt(num / 26);
  let numberMod = num % 26
  let output = ""
  for (let i = 0; i < numberOfLetters; i++) {
    output += "A"
  }
  output += String.fromCharCode(65 + numberMod)
  return output
}



    //cleanup code
    //fs.rmdirSync(tempdownloads, { recursive: true }); 



// TODO
// GenerateCleanSheet.js
// ParsePairPorts.js --> Exports to {location, date, amount, fee}
// ParseColombus(file) --> Exports to {location, date, amount, fee}
// GenerateExports(filename) --> Combines parsed + sheets and exports


// Changelog
// [10/3/24] 1.7.2 - Added Transactions value, Allows for fees to be calculated differently
// [10/11/22] 1.7.1 - Added MANANA GROCERY back, Removed MANANA GROCERY Fees
// [10/7/22] 1.7 - Removed MANANA GROCERY from showing up on sheet as the data is not needed.
// [4/3/22] 1.6 - Fixed --no-sandbox issue with linux distributions
// [7/7/20] 1.5 - Second Columbus account disabled. Forever young fruit now gives 1.10 per transaction (40%)
// [4/8/20] 1.4 - Pairports functionality removed
// [2/2/20] 1.3 - Made paireports resize column (Uncomment Line 86)
// [2/2/20] 1.2 - Disabled deleting code (Not working on other computer), Fixed month to show previous month instead of current

// Note to self
// This application is working as of node versions

/*
* v14.17.0
* v16.3.0
*/